<?php

/*
 * As a Developer, you should normally deal with code that it has not been 
 * developed by you. That legacy code is found on every project where 
 * a lot developers have put their hands on.
 * 
 * You receive an Issue that have the following description:
 * "Our normalization script needs to normalize an additional word:
 * 'g3nd3r'
 * This word should be normalized to 'gender'"
 *
 * The following lines are from the script the Issue is refering to.
 * You must refactor it to be more maintainable in order to reduce the time 
 * spent on it when more normalized words will be added.
 *
 */

 /* con esta refactorización elijo la sentencia switch por encima de la sentencia if, para que si encuentra el valor buscado, detenga su búsqueda, pensando en la eficiencia del código para la resolución del algoritmo */

function normalizeLabel($k) {
    switch ($k) {
        case "d3scr1pt10n":
            $k = "description";
            break;
        case "n4m3":
            $k = "name";
            break;
        case "4g3":
            $k = "age";
            break;
        case "4ddr3ss":
            $k = "address";
            break;
        case "c1t1":
            $k = "city";
            break;
        case "c0untr1":
            $k = "country";
            break;
        case "ph0n3":
            $k = "phone";
            break;
        case "4v4t4r":
            $k = "avatar";
            break;
        case "g3nd3r":
            $k = "gender";
            break;
        default:
            $k = "No se encontró normalización para la palabra: " . $k;
    }

    return $k;
}

// This part is to just check the script continues working
// Create a few unit tests to validate your script works fine
$strangeLabels = [
  "d3scr1pt10n",
  "n4m3",
  "4g3",
  "4ddr3ss",
  "c1t1",
  "c0untr1",
  "ph0n3",
  "4v4t4r",
  "g3nd3r"
];

foreach ($strangeLabels as $strangeLabel) {
  echo normalizeLabel($strangeLabel)."\n";
}

?>