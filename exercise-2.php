<?php

/*
 *
 * Refactor the 'for' loop included in the following method to be only
 * a max of two lines inside it and outputs the same results.
 *
 */

// validar parametros y construir la url
function validateParams($param, $nonSpecialParams, $url, $parsedUrl) {
    if (isset($parsedUrl[$param])) {
        switch ($param) {
            case 'scheme':
                $url .= $parsedUrl[$param] . '://';
                break;
            case 'port':
                $url .= ':' . $parsedUrl[$param];
                break;
            case 'query':
                $url .= '?' . $parsedUrl[$param];
                break;
            case 'fragment':
                $url .= '#' . $parsedUrl[$param];
                break;
            case 'pass':
                $url .= ':' . $parsedUrl[$param] . '@';
                break;
            case (in_array($param, $nonSpecialParams)):
                $url .= $parsedUrl[$param];
                break;
            default:
                $url .= '';
                break;
        }
    } 

    return $url;
}

// analizar e imprimir la url
function unparseUrl($parsedUrl)
{
    $params = [
        'scheme',
        'user',
        'pass',
        'host',
        'port',
        'path',
        'query',
        'fragment',
    ];
    $nonSpecialParams = [
        'host',
        'user',
        'path',
    ];
    $url = '';

    foreach ($params as $param) {
        echo validateParams($param, $nonSpecialParams, $url, $parsedUrl);
    }
}

// Example to check the method.
// The result should be 'https://test-user:test-password@localhost:8100/blog/post/1?lang=en#this-reference'
//
// Create a few unit tests to validate your script works fine
$parsedUrl = [
  'scheme' => 'https',
  'user' => 'test-user',
  'pass' => 'test-password',
  'host' => 'localhost',
  'port' => '8100',
  'path' => '/blog/post/1',
  'query' => 'lang=en',
  'fragment' => 'this-reference',
];

echo unparseUrl($parsedUrl) . "\n";

?>