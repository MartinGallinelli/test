<?php

/*
 * Create an Utilities class that will have a private property as the array
 * shown on the following lines, and will have a public method that allows
 * you to access for any property value in the array by using the JS
 * accessing syntax.
 *
 */

require_once('Utilities.php');
// creo una instancia de la clase
$utilities = new Utilities;

// should return "you're great!"
echo $utilities->getArrayValueByStringKey('level1_1.level2_1.level3_1.level4_1') . "\n";

// should return "good!"
echo $utilities->getArrayValueByStringKey('level1_1.level2_2.level3_1') . "\n";

// should return "cool :D"
echo $utilities->getArrayValueByStringKey('level1_2') . "\n";
