<?php
/*
 * Open each files and follow the instructions.
 * You could check the results for all of them on the right side.
 * Also add Unit Tests in Tests/ folder accordingly.
 */

echo "-------------------------\n";
echo "First exercise output:";
echo "\n-------------------------\n";
require_once('exercise-1.php');
echo "\n-------------------------\n";
echo "Second exercise output:";
echo "\n-------------------------\n";
require_once('exercise-2.php');
echo "\n-------------------------\n";
echo "Third exercise output:";
echo "\n-------------------------\n";
require_once('exercise-3.php');
echo "\n-------------------------\n";