<?php

class Utilities {

    private $data = [
        'level1_1' => [
            'level2_1' => [
                'level3_1' => [
                    'level4_1' => 'you\'re great!',
                ],
            ],
            'level2_2' => [
                'level3_1' => 'good!',
                'level3_2' => 'amazing!'
            ]
        ],
        'level1_2' => 'cool :D'
    ];

    public function __construct()
    {        
    }

    public function getArrayValueByStringKey($key) {
        // separo las palabras por .
        $k = explode(".", $key);
        
        // analizo la cantidad de elementos del arreglo
        switch (count($k)) {
            case 1:
                return $this->data[$k[0]];
            case 2:
                return $this->data[$k[0]][$k[1]];
            case 3:
                return $this->data[$k[0]][$k[1]][$k[2]];
            case 4:
                return $this->data[$k[0]][$k[1]][$k[2]][$k[3]];
        }
    }
}